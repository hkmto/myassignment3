﻿using MyAssignment3.DTO.CharacterDTO;
using MyAssignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.Services
{
    public interface ICharacterService
    {
        public Task<IEnumerable<Character>> GetALLCharactersAsync();
        public Task<Character> GetSpescificCharacterAsync(int id);
        public Task<IEnumerable<Character>> GetMoviesForSpesificCharacterAsync(int id);
        public Task UpdateCharacterAsync(Character character);
        public Task<Character> AddCharacterAsync(Character character);
        public Task DeleteCharacterAsync(int id);
        public bool CharacterExsists(int id);


    }
}
