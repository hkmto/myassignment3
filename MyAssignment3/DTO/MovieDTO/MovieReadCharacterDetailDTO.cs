﻿using System.Collections.Generic;

namespace MyAssignment3.DTO.MovieDTO
{
    public class MovieReadCharacterDetailDTO
    {
        public int MovieId { get; set; }
       
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
       
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
     
        public string Picture { get; set; }

        public string Trailer { get; set; }
        public int FranchiseId { get; set; }

        public List<string> Characters { get; set; }
    }
}