﻿using MyAssignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetSpescificMovieAsync(int id);
        public Task<IEnumerable<Movie>> GetCharactersForSpecificMovieAsync(int id);
        public Task UpdateCharacterAsync(Movie movie);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task DeleteMovieAsync(int id);
        public Task UpdateCharactersInMovieAsync(int id, List<int> characters);
        public bool MovieExsists(int id);
    }
}
