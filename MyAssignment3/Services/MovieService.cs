﻿using Microsoft.EntityFrameworkCore;
using MyAssignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.Services
{
    public class MovieService : IMovieService
    {
        private readonly MoviesDbContext _context;

        public MovieService(MoviesDbContext context)
        {
            _context = context;
        }

        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;

        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies.ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetCharactersForSpecificMovieAsync(int id)
        {
            return await _context.Movies
                .Include(movie => movie.Characters).Where(m => m.MovieId == id)
                .ToListAsync();
        }

        public async Task<Movie> GetSpescificMovieAsync(int id)
        {
            return await _context.Movies.FindAsync(id);
        }

        public bool MovieExsists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }

        public async Task UpdateCharacterAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateCharactersInMovieAsync(int id, List<int> characters)
        {
            Movie movieToUpdateCharacters = await _context.Movies
                .Include(movie => movie.Characters)
                .Where(movie => movie.MovieId == id).FirstAsync();

            List<Character> movieCharacters = new();
            foreach (var characterId in characters)
            {
                Character movieCharacter = await _context.Characters.FindAsync(characterId);
                if (movieCharacter == null)

                    throw new KeyNotFoundException();
                movieCharacters.Add(movieCharacter);


                movieToUpdateCharacters.Characters = movieCharacters;
                await _context.SaveChangesAsync();

            }
        }
    }
}
