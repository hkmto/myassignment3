﻿using MyAssignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.DTO.FranchiseDTO
{
    public class FranchiseReadDTO
    {
        public int FranchiseId { get; set; }
      
        public string Name { get; set; }
        
       
    }
}
