﻿using Microsoft.EntityFrameworkCore;
using MyAssignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MoviesDbContext _context;

        public FranchiseService(MoviesDbContext context)
        {
            _context = context;
        }
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExsists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }

        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises.ToListAsync();
        }

        public async Task<IEnumerable<Character>> GetCharactersInFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);

            
            var movies = await _context.Movies
            .Include(movies => movies.Characters).Where(c => c.FranchiseId == id)
            .ToListAsync();

            var characters = new List<Character>();
            foreach (var movie in movies)
            {
                foreach (var character in movie.Characters.ToList())
                {
                    if (!characters.Contains(character))
                    {
                        characters.Add(character);
                    }
                }
            }



            return characters;


        }
        

        public async Task<IEnumerable<Franchise>> GetMoviesForSpecificFranchiseAsync(int id)
        {
            return await _context.Franchises
                    .Include(c => c.Movies).Where(c => c.FranchiseId == id)
                    .ToListAsync();
        }

        public async Task<Franchise> GetSpescificFranchiseAsync(int id)
        {
           return await _context.Franchises.FindAsync(id);
        }

        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMoviesInFranchiseAsync(int id, List<int> movies)
        {

            Franchise franchiseToUpdateMovies = await _context.Franchises
                .Include(franchise => franchise.Movies)
                .Where(franchise => franchise.FranchiseId == id).FirstAsync();

            List<Movie> franchiseMovies = new();
            foreach (var movieId in movies)
            {
                Movie franchiseMovie = await _context.Movies.FindAsync(movieId);
                if (franchiseMovie == null)

                    throw new KeyNotFoundException();
                franchiseMovies.Add(franchiseMovie);


                franchiseToUpdateMovies.Movies = franchiseMovies;
                await _context.SaveChangesAsync();
            }
        }
    }
}
