﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.Models
{
    public class Franchise
    {
        public int FranchiseId { get; set; }
        [Required]
        public string Name { get; set; }
        [MaxLength(50)]
        public string Description { get; set; }

        //Relationships
        public ICollection<Movie> Movies { get; set; }

    }
}
