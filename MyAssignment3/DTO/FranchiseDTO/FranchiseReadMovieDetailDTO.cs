﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.DTO.FranchiseDTO
{
    public class FranchiseReadMovieDetailDTO
    {

        public int FranchiseId { get; set; }

        public string Name { get; set; }

        public List<int> Movies { get; set; }
    }

}