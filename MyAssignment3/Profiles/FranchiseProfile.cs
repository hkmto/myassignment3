﻿using AutoMapper;
using MyAssignment3.DTO.FranchiseDTO;
using MyAssignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.DTO.MovieDTO
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        { 

            CreateMap<Franchise, FranchiseReadMovieDetailDTO>()
                .ForMember(cdto => cdto.Movies, option => option
                .MapFrom(c => c.Movies.Select(c =>c.MovieId)
                .ToArray())).ReverseMap();
            CreateMap<Franchise, FranchiseReadDTO>();
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseUpdateDTO, Franchise>();
            
        }
    }
}
