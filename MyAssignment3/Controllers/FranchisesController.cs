﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyAssignment3.DTO.CharacterDTO;
using MyAssignment3.DTO.FranchiseDTO;
using MyAssignment3.Models;
using MyAssignment3.Services;

namespace MyAssignment3.Controllers
{
    [Route("api/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _mapper = mapper;
            
        }

        /// <summary>
        ///  Get all franchises from the database
        /// </summary>
        /// <returns>List of franchise objects in json format</returns>
        /// <response code="200">Sucessfully returns franchise object</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            var franchise = await _franchiseService.GetAllFranchisesAsync();
            List<FranchiseReadDTO> franchiseReadDto = _mapper.Map<List<FranchiseReadDTO>>(franchise);

            return franchiseReadDto;
        }

        /// <summary>
        /// Get a specific franchise for a given ID 
        /// </summary>
        /// <param name="id">ID for the franchise to be retrieved</param>
        /// <returns>A franchise object in json format</returns>
        /// <response code="200">Sucessfully returns a franchise object for the given ID</response>
        /// <response code="404">No franchise object was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchiseService.GetSpescificFranchiseAsync(id);
            var franchiseReadDto = _mapper.Map<FranchiseReadDTO>(franchise);

            if (franchise == null)
            {
                return NotFound();
            }

            return franchiseReadDto;
        }

        /// <summary>
        /// Updates a franchise for a gived ID
        /// </summary>
        /// <param name="id">ID for the franchise object to be retrieved</param>
        /// <param name="franchiseDto">The franchise dto object to be updated</param>
        /// <returns>An updated franchise object</returns>
        /// <response code="200">Franchise object was sucsessfully updated</response>
        /// <response code="400">Bad request: incorrect franchise object format</response>
        /// <response code="404">No franchise object was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseUpdateDTO franchiseDto)
        {
            if (id != franchiseDto.FranchiseId)
            {
                return BadRequest();
            }
            var franchise = _mapper.Map<Franchise>(franchiseDto);
            await _franchiseService.UpdateFranchiseAsync(franchise);

         
            {
                if (!_franchiseService.FranchiseExsists(id))
                {
                    return NotFound();
                }
              
            }

            return NoContent();
        }
        /// <summary>
        /// Gets all movies in a franchise for a given ID 
        /// </summary>
        /// <param name="id">ID for the franchise object to be retrieved</param>
        /// <returns>A franchise object in json format </returns>
        /// <response code="200">Sucessfully returns movies in a specified franchise object</response>
        /// <response code="404">No franchise was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<FranchiseReadMovieDetailDTO>>> GetMoviesInFranchise(int id)
        {
            var franchise = await _franchiseService.GetSpescificFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<List<FranchiseReadMovieDetailDTO>>(await _franchiseService.GetMoviesForSpecificFranchiseAsync(id)));
        }


        /// <summary>
        /// Creates a new franchise in the database
        /// </summary>
        /// <param name="franchiseDto">The franchise dto object to be created</param>
        /// <returns>The created franchise object</returns>
        /// <response code="201">Franchise object was sucsessfully created </response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO franchiseDto)
        {
            var franchise = _mapper.Map<Franchise>(franchiseDto);
           await _franchiseService.AddFranchiseAsync(franchise);

            return CreatedAtAction("GetFranchise", new { id = franchise.FranchiseId }, _mapper.Map<FranchiseReadDTO>(franchise));
        }

        /// <summary>
        /// Deletes a franchise from the database for the given ID
        /// </summary>
        /// <param name="id">ID for the franchise object to be deleted</param>
        /// <returns>Nothing is returned</returns>
        /// <response code="204">Franchise object has been sucsessfully deleted</response>
        /// <response code="404">The franchise object to be deleted was not found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            
            if (!_franchiseService.FranchiseExsists(id))
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);

           
            return NoContent();
        }

        /// <summary>
        /// Update movies in a franchise
        /// </summary>
        /// <param name="id">Id for the franchise object to be updated</param>
        /// <param name="movies">ID's for the movie objects in the franchise to be updated</param>
        /// <returns>Updated movie objects in franchise</returns>
        ///  <response code="200">Sucessfully update movie objects in franchise</response>
        ///  <response code="400">Bad request: incorrect franchise object format</response>
        /// <response code="404">No franchise object was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMoviesInFranchise(int id, List<int> movies)
        {
            if (!_franchiseService.FranchiseExsists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.UpdateMoviesInFranchiseAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid movie");

            }

            
            return NoContent();
        }
        /// <summary>
        /// Gets all characters in a franchise
        /// </summary>
        /// <param name="id">ID for the franchise object to be retrieved</param>
        /// <returns>Franchise object json format</returns>
        ///  <response code="200">Sucessfully retrieves characters in franchise</response>
        /// <response code="404">No franchise object was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInFranchise(int id)
        {
            
            if (!_franchiseService.FranchiseExsists(id)) 
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(await _franchiseService.GetCharactersInFranchiseAsync(id));
        
        }

        
    }
}
