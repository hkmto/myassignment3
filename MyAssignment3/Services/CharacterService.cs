﻿using Microsoft.EntityFrameworkCore;
using MyAssignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.Services
{
    public class CharacterService : ICharacterService
    {
        private readonly MoviesDbContext _context;

        public CharacterService(MoviesDbContext context)
        {
            _context = context;
        }

        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        public bool CharacterExsists(int id)
        {
            return _context.Characters.Any(e => e.CharacterId == id);
        }

        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
            
        }

        public async Task<IEnumerable<Character>> GetALLCharactersAsync()
        {
            return await _context.Characters.ToListAsync();
        }

        public async Task<IEnumerable<Character>> GetMoviesForSpesificCharacterAsync(int id)
        {
          
           return await _context.Characters
                 .Include(c => c.Movies).Where(c => c.CharacterId == id)
                 .ToListAsync();
            
        }

        public async Task<Character> GetSpescificCharacterAsync(int id)
        {
            return await _context.Characters.FindAsync(id);
        }

        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
