﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.Models
{
    public class SeedHelper
    {

        public static IEnumerable<Franchise> GetFranchiseSeeds()
        {
            IEnumerable<Franchise> seedFranchise = new List<Franchise>()
            {
                new Franchise {FranchiseId=1,Name = "Marvel", Description = "X-Men Collection"},
                new Franchise {FranchiseId=2,Name = "Universial Pictures", Description = "Jason Bourne collection"},
                new Franchise {FranchiseId=3, Name = "LucasFilm Ltd", Description = "Star Wars collection" }

            };


            return seedFranchise;


        }

        public static IEnumerable<Movie> GetMovieSeeds()
        {
            IEnumerable<Movie> seedMovie = new List<Movie>()
            {
                new Movie {MovieId=1,MovieTitle="A new Hope",Genre="Adventure",ReleaseYear=1977,Director="George Lucas",
                           Picture="https://www.imdb.com/title/tt0076759/?ref_=nv_sr_srsg_1",
                           Trailer="https://www.imdb.com/video/vi1317709849?playlistId=tt0076759&ref_=tt_ov_vi",FranchiseId=3},
                
                new Movie {MovieId=2,MovieTitle="The Empir Strikes Back",Genre="Adventure",ReleaseYear=1980,Director="George Lucas",
                           Picture="https://www.imdb.com/title/tt0080684/?ref_=nv_sr_srsg_0",
                           Trailer="https://www.imdb.com/video/vi221753881?playlistId=tt0080684&ref_=tt_ov_vi",FranchiseId=3},
                
                new Movie {MovieId=3,MovieTitle="X-Men The Last Stand",Genre="Adventure",ReleaseYear=2006,Director="Brett Ratner",
                           Picture="https://www.imdb.com/title/tt0376994/?ref_=nv_sr_srsg_0",
                           Trailer="https://www.imdb.com/video/vi3890348313?playlistId=tt0376994&ref_=tt_ov_vi", FranchiseId=1},
                new Movie {MovieId=4,MovieTitle="X-Men First Class",Genre="Adventure",ReleaseYear=2011,Director="Matthew Vaughn",
                           Picture="https://www.imdb.com/title/tt1270798/?ref_=nv_sr_srsg_0",
                           Trailer="https://www.imdb.com/video/vi168926489?playlistId=tt1270798&ref_=tt_pr_ov_vi",FranchiseId=1},
                
                new Movie {MovieId=5,MovieTitle="The Bourne Identity",Genre="Action",ReleaseYear=2002,Director="Doug Liman",
                           Picture="https://www.imdb.com/title/tt0258463/?ref_=nv_sr_srsg_0",
                           Trailer="https://www.imdb.com/video/vi680919321?playlistId=tt0258463&ref_=tt_ov_vi",FranchiseId=2},
                new Movie {MovieId=6,MovieTitle="Jason Bourne Ultimatum",Genre="Action",ReleaseYear=2007,Director="Paul Greengrass",
                           Picture="https://www.imdb.com/title/tt0440963/?ref_=nv_sr_srsg_7",
                           Trailer="https://www.imdb.com/video/vi221753881?playlistId=tt0080684&ref_=tt_ov_vi",FranchiseId=2},
            };


            return seedMovie;

        }


        public static IEnumerable<Character> GetCharacterSeeds()
        {
            IEnumerable<Character> seedCharacter = new List<Character>()
            {
               new Character{CharacterId=1,FullName="Matt Damon",Alias="Mr.Niceguy",Gender="M",Picture="https://www.imdb.com/name/nm0000354/?ref_=fn_al_nm_1"},
               new Character{CharacterId=2,FullName="Harrison Ford",Alias="Adventure guy",Gender="M",Picture="https://www.imdb.com/name/nm0000148/?ref_=fn_al_nm_1"},
               new Character{CharacterId=3,FullName="Hugh Jacmann",Alias="The Aussie",Gender="M",Picture="https://www.imdb.com/name/nm0413168/?ref_=nv_sr_srsg_0"},
               new Character{CharacterId=4,FullName="Halle Berry",Alias="Ice woman",Gender="K",Picture="https://www.imdb.com/name/nm0000932/?ref_=nv_sr_srsg_0"},
               new Character{CharacterId=5,FullName="Carrie Fisher",Alias="Galactic princess",Gender="K",Picture="https://www.imdb.com/name/nm0000402/?ref_=tt_cl_t_3"},
               new Character{CharacterId=6,FullName="Julia Stiles",Alias="Nice girl",Gender="k",Picture="https://www.imdb.com/name/nm0005466/?ref_=tt_cl_t_10"}
            };


            return seedCharacter;

        }


        public static IEnumerable<object> GetLinkingTableSeeds()
        {
            IEnumerable<object> seedLinkingTable = new List<object>()
            {
                        new { MovieId= 1, CharacterId = 5 },
                        new { MovieId = 1, CharacterId = 2 },
                        new { MovieId = 2, CharacterId = 5 },
                        new { MovieId = 3, CharacterId = 3 },
                        new { MovieId = 4, CharacterId = 3 },
                        new { MovieId = 5, CharacterId = 1 },
                        new { MovieId = 5, CharacterId = 6 }

            };


            return seedLinkingTable;

        }


    }
}
