﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.Models
{
    public class Character
    {
      
        public int CharacterId { get; set; }
        [Required]
        [MaxLength(50)]
        public string FullName { get; set; }
        public string Alias { get; set; }
        [StringLength(1)]
        [RegularExpression("m|k/gi",ErrorMessage ="Specify only one letter for gender")]
        public string Gender { get; set; }
        [Url]
        public string Picture { get; set; }

        // Relationships

        public ICollection<Movie> Movies { get; set; }

    }
}
