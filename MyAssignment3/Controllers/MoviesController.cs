﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyAssignment3.DTO.MovieDTO;
using MyAssignment3.Models;
using MyAssignment3.Services;

namespace MyAssignment3.Controllers
{
    [Route("api/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;

        public MoviesController(IMovieService movieService, IMapper mapper)
        {
            _movieService = movieService;
            _mapper = mapper;
        }


        /// <summary>
        ///  Gets all movies from the database
        /// </summary>
        /// <returns>Movie objects in json format</returns>
        /// <response code="200">Sucessfully returns movie objects</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            var movie = await _movieService.GetAllMoviesAsync();
            var movieReadDto = _mapper.Map<List<MovieReadDTO>>(movie);

            return Ok(movieReadDto);
        }

        /// <summary>
        /// Gets a specific movie for a given ID
        /// </summary>
        /// <param name="id">ID for the movie object to be retrieved</param>
        /// <returns>Movie object in json format</returns>
        /// <response code="200">Sucessfully returns a movie object for the given ID</response>
        /// <response code="404">No movie object was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {

            var movie = await _movieService.GetSpescificMovieAsync(id);
            var movieReadDto = _mapper.Map<MovieReadDTO>(movie);

            if (movie == null)
            {
                return NotFound();
            }

            return Ok(movieReadDto);
        }
        /// <summary>
        /// Gets all characters in a movie for a given ID
        /// </summary>
        /// <param name="id">ID of the movie object to be returned</param>
        /// <returns>Movie object json format</returns>
        /// <response code="200">Sucessfully returns all characters for a specified movie object</response>
        /// <response code="404">No movie object was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<MovieReadCharacterDetailDTO>>> GetCharacterInMovies(int id)
        {
            var movie = await _movieService.GetSpescificMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<List<MovieReadCharacterDetailDTO>>(await _movieService.GetCharactersForSpecificMovieAsync(id)));
        }

        /// <summary>
        /// Updates a specific movie for a given ID
        /// </summary>
        /// <param name="id">ID for the movie object to be updated</param>
        /// <param name="movieDto">The movie dto object to be updated</param>
        /// <returns>An updated movie</returns>
        /// <response code="200">Movie object was sucsessfully updated</response>
        /// <response code="400">Bad request: incorrect movie object format</response>
        /// <response code="404">No movie object was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieUpdateDTO movieDto)
        {
            if (id!=movieDto.MovieId)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExsists(id))
                {
                    return NotFound();
                }
            var movie = _mapper.Map<Movie>(movieDto);
            await _movieService.UpdateCharacterAsync(movie);
       
            return NoContent();
        }

        /// <summary>
        /// Adds a movie to the database
        /// </summary>
        /// <param name="movieDto">Movie dto object to be created</param>
        /// <returns>The created movie object</returns>
        /// <response code="201">Movie object was sucsessfully created </response>
        [ProducesResponseType(StatusCodes.Status201Created)]

        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO movieDto)
        {
            var movie = _mapper.Map<Movie>(movieDto);
            await _movieService.AddMovieAsync(movie);

            return CreatedAtAction("GetMovie", new { id = movie.MovieId },_mapper.Map<MovieReadDTO>(movie));
        }

        /// <summary>
        /// Deletes a movie from the database
        /// </summary>
        /// <param name="id">ID of movie object to be deleted</param>
        /// <returns>Nothing is returned</returns>
        /// <response code="204">Movie object has been sucsessfully deleted</response>
        /// <response code="404">The movie object to be deleted was not found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
          
            if (!_movieService.MovieExsists(id))
            {
                return NotFound();
            }
            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Updates characters in a movie for a given id
        /// </summary>
        /// <param name="id">Id for the movie to be updated</param>
        /// <param name="characters">ID's for the characters to be updated</param>
        /// <returns>Updated movie object</returns>
        ///  <response code="200">Sucessfully updates characters for a specific movie object</response>
        ///  <response code="400">Bad request: incorrect movie object format</response>
        /// <response code="404">No movie object was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateCharactersInMovie(int id, List<int>characters)
        {
            if (!_movieService.MovieExsists(id))
            {
                return NotFound();
            }
                          
            try
            {
                await _movieService.UpdateCharactersInMovieAsync(id, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid character");

            }

            return NoContent();
        }
    }
}
