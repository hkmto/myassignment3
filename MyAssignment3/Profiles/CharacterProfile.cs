﻿using AutoMapper;
using MyAssignment3.DTO.CharacterDTO;
using MyAssignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.DTO.MovieDTO
{
    public class CharacterProfile :Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadMovieDetailDTO>()
                .ForMember(cdto => cdto.Movies, option => option
                .MapFrom(c => c.Movies.Select(c=>c.MovieTitle)
                .ToArray())).ReverseMap();
            CreateMap<Character, CharacterReadDTO>();
            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterUpdateDTO, Character>();
            
        }
    }
}
