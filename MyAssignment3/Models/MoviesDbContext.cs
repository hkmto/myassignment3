﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.Models
{
    public class MoviesDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }
        public DbSet<Character> Characters { get; set; }
        public MoviesDbContext(DbContextOptions options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchiseSeeds());
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovieSeeds());
            modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacterSeeds());

            modelBuilder.Entity<Movie>()
            .HasMany(p => p.Characters)
            .WithMany(m => m.Movies)
            .UsingEntity<Dictionary<string, object>>(
                "Movie_Characters",
                r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                je =>
                {
                    je.HasKey("MovieId", "CharacterId");
                    je.HasData(SeedHelper.GetLinkingTableSeeds()


                    ); 
                });
        }
    }
}
