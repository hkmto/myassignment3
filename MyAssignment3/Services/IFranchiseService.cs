﻿using MyAssignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> GetSpescificFranchiseAsync(int id);
        public Task<IEnumerable<Franchise>> GetMoviesForSpecificFranchiseAsync(int id);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task DeleteFranchiseAsync(int id);
        public Task UpdateMoviesInFranchiseAsync(int id, List<int> movies);
        public Task<IEnumerable<Character>> GetCharactersInFranchiseAsync(int id); 
        public bool FranchiseExsists(int id);
    }
}
