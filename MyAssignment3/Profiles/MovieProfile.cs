﻿using AutoMapper;
using MyAssignment3.DTO.MovieDTO;
using MyAssignment3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.DTO.MovieDTO
{
    public class MovieProfile :Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadCharacterDetailDTO>()
                    .ForMember(movie => movie.Characters, option => option
                    .MapFrom(m => m.Characters.Select(m => m.FullName)
                    .ToArray())).ReverseMap();
            CreateMap<Movie, MovieReadDTO>();
            CreateMap<MovieCreateDTO, Movie>();
            CreateMap<MovieUpdateDTO, Movie>();
          
        }
    }
}
