﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.DTO.MovieDTO
{
    public class MovieCreateDTO
    {
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
    
        public int ReleaseYear { get; set; }
        public string Director { get; set; }

        public string Picture { get; set; }
      
        public string Trailer { get; set; }
        public int FranchiseId { get; set; }
    }
}
