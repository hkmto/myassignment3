﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyAssignment3.Models;

namespace MyAssignment3.DTO.CharacterDTO
{
    public class CharacterReadDTO
    {
        public int CharacterId { get; set; }
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
      
    }
}
