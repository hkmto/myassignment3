﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyAssignment3.Models
{
    public class Movie
    {
        public int MovieId { get; set; }
        [Required]
        [MaxLength(50, ErrorMessage ="The entered movie title {0} is too long")]
        public string MovieTitle { get; set; }
        public string  Genre { get; set; }
        [RegularExpression("^\\d{4}")]
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        [Url]
        public string Picture { get; set; }
        [Url]
        public string Trailer { get; set; }

        //foreign key and relationships
        public int FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<Character> Characters { get; set; }

    }
}
