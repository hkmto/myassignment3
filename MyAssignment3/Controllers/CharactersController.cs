﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyAssignment3.DTO.CharacterDTO;
using MyAssignment3.Models;
using MyAssignment3.Services;

namespace MyAssignment3.Controllers
{
    [Route("api/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public CharactersController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }

        /// <summary>
        ///  Gets all characters from the database
        /// </summary>
        /// <returns>List of characters objects in json format</returns>
        /// <response code="200">Sucessfully returns character objects</response>
        [ProducesResponseType(StatusCodes.Status200OK)]

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            var character = await _characterService.GetALLCharactersAsync();
            var characterReadDto = _mapper.Map<List<CharacterReadDTO>>(character);
            return Ok(characterReadDto);
        }


        /// <summary>
        /// Gets a specific character for a given ID
        /// </summary>
        /// <param name="id">ID for the character to be retrieved</param>
        /// <returns>A character object in json format</returns>
        /// <response code="200">Sucessfully returns a character object for the given ID</response>
        /// <response code="404">No character object was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _characterService.GetSpescificCharacterAsync(id);
            var characterReadDto = _mapper.Map<CharacterReadDTO>(character);
            if (character == null)
            {
                return NotFound();
            }

            return Ok(characterReadDto);
        }

        /// <summary>
        /// Gets all movies for a specific character
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Character object json format</returns>
        /// <response code="200">Sucessfully returns the movies for the specified character object</response>
        /// <response code="404">No character object was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
       
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<CharacterReadMovieDetailDTO>>> GetCharacterInMovies(int id)
        {
            var character = await _characterService.GetSpescificCharacterAsync(id);
          
            if (character == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<List<CharacterReadMovieDetailDTO>>(await _characterService.GetMoviesForSpesificCharacterAsync(id)));
        }


        /// <summary>
        /// Updates a character for a given ID
        /// </summary>
        /// <param name="id">ID of the character object to be updated</param>
        /// <param name="dtocharacter">The dto object of the character to be updated</param>
        /// <returns>An updated movie object</returns>
        /// <response code="200">Character object was sucsessfully updated</response>
        /// <response code="400">Bad request: incorrect character object format</response>
        /// <response code="404">No character object was found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]

        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterUpdateDTO dtocharacter)
        {
            if (id != dtocharacter.CharacterId)
            {
                return BadRequest();
            }

                if (!_characterService.CharacterExsists(id))
                {
                    return NotFound();
                }
               
                Character character = _mapper.Map<Character>(dtocharacter);
                await _characterService.UpdateCharacterAsync(character);

            return NoContent();
        }

        /// <summary>
        /// Adds a character to the database
        /// </summary>
        /// <param name="dtocharacter">The character dto object to be created</param>
        /// <returns>The created movie object</returns>
        /// <response code="201">Character object was sucsessfully created </response>
        [ProducesResponseType(StatusCodes.Status201Created)]

        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO dtocharacter)
        {
            var character = _mapper.Map<Character>(dtocharacter);
            await _characterService.AddCharacterAsync(character);

            return Ok(CreatedAtAction("GetCharacter", new { id = character.CharacterId }, 
                _mapper.Map<CharacterReadDTO>(character)));
        }

        /// <summary>
        /// Deletes a character from the database for given ID
        /// </summary>
        /// <param name="id">ID of the character object to be deleted</param>
        /// <returns>Nothing is returned</returns>
        /// <response code="204">Character object has been sucsessfully deleted</response>
        /// <response code="404">The character object to be deleted was not found for the given ID</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCharacter(int id)
        {
            
          
         
            if (!_characterService.CharacterExsists(id))
            {
                return NotFound();
            }

            await _characterService.DeleteCharacterAsync(id);

            return NoContent();
        }

     
    }
}
