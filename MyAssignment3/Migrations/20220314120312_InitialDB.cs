﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyAssignment3.Migrations
{
    public partial class InitialDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Characters",
                columns: table => new
                {
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Alias = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Gender = table.Column<string>(type: "nvarchar(1)", maxLength: 1, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Characters", x => x.CharacterId);
                });

            migrationBuilder.CreateTable(
                name: "Franchises",
                columns: table => new
                {
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchises", x => x.FranchiseId);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieTitle = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Genre = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReleaseYear = table.Column<int>(type: "int", nullable: false),
                    Director = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Trailer = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FranchiseId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.MovieId);
                    table.ForeignKey(
                        name: "FK_Movies_Franchises_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchises",
                        principalColumn: "FranchiseId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Movie_Characters",
                columns: table => new
                {
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    CharacterId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie_Characters", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_Movie_Characters_Characters_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Characters",
                        principalColumn: "CharacterId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Movie_Characters_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "CharacterId", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Mr.Niceguy", "Matt Damon", "M", "https://www.imdb.com/name/nm0000354/?ref_=fn_al_nm_1" },
                    { 2, "Adventure guy", "Harrison Ford", "M", "https://www.imdb.com/name/nm0000148/?ref_=fn_al_nm_1" },
                    { 3, "The Aussie", "Hugh Jacmann", "M", "https://www.imdb.com/name/nm0413168/?ref_=nv_sr_srsg_0" },
                    { 4, "Ice woman", "Halle Berry", "K", "https://www.imdb.com/name/nm0000932/?ref_=nv_sr_srsg_0" },
                    { 5, "Galactic princess", "Carrie Fisher", "K", "https://www.imdb.com/name/nm0000402/?ref_=tt_cl_t_3" },
                    { 6, "Nice girl", "Julia Stiles", "k", "https://www.imdb.com/name/nm0005466/?ref_=tt_cl_t_10" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "FranchiseId", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "X-Men Collection", "Marvel" },
                    { 2, "Jason Bourne collection", "Universial Pictures" },
                    { 3, "Star Wars collection", "LucasFilm Ltd" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genre", "MovieTitle", "Picture", "ReleaseYear", "Trailer" },
                values: new object[,]
                {
                    { 3, "Brett Ratner", 1, "Adventure", "X-Men The Last Stand", "https://www.imdb.com/title/tt0376994/?ref_=nv_sr_srsg_0", 2006, "https://www.imdb.com/video/vi3890348313?playlistId=tt0376994&ref_=tt_ov_vi" },
                    { 4, "Matthew Vaughn", 1, "Adventure", "X-Men First Class", "https://www.imdb.com/title/tt1270798/?ref_=nv_sr_srsg_0", 2011, "https://www.imdb.com/video/vi168926489?playlistId=tt1270798&ref_=tt_pr_ov_vi" },
                    { 5, "Doug Liman", 2, "Action", "The Bourne Identity", "https://www.imdb.com/title/tt0258463/?ref_=nv_sr_srsg_0", 2002, "https://www.imdb.com/video/vi680919321?playlistId=tt0258463&ref_=tt_ov_vi" },
                    { 6, "Paul Greengrass", 2, "Action", "Jason Bourne Ultimatum", "https://www.imdb.com/title/tt0440963/?ref_=nv_sr_srsg_7", 2007, "https://www.imdb.com/video/vi221753881?playlistId=tt0080684&ref_=tt_ov_vi" },
                    { 1, "George Lucas", 3, "Adventure", "A new Hope", "https://www.imdb.com/title/tt0076759/?ref_=nv_sr_srsg_1", 1977, "https://www.imdb.com/video/vi1317709849?playlistId=tt0076759&ref_=tt_ov_vi" },
                    { 2, "George Lucas", 3, "Adventure", "The Empir Strikes Back", "https://www.imdb.com/title/tt0080684/?ref_=nv_sr_srsg_0", 1980, "https://www.imdb.com/video/vi221753881?playlistId=tt0080684&ref_=tt_ov_vi" }
                });

            migrationBuilder.InsertData(
                table: "Movie_Characters",
                columns: new[] { "CharacterId", "MovieId" },
                values: new object[,]
                {
                    { 3, 3 },
                    { 3, 4 },
                    { 1, 5 },
                    { 6, 5 },
                    { 5, 1 },
                    { 2, 1 },
                    { 5, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movie_Characters_CharacterId",
                table: "Movie_Characters",
                column: "CharacterId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_FranchiseId",
                table: "Movies",
                column: "FranchiseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Movie_Characters");

            migrationBuilder.DropTable(
                name: "Characters");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Franchises");
        }
    }
}
